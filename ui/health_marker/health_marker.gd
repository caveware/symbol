tool
extends Node2D

# Exported variable
export var active = false setget set_active

# Performed to set or unset activity
func set_active(value):

	# Firstly, set the current prop
	active = value

	# Secondly, check to see that we're in the tree with our nodes
	if has_node("empty") and has_node("full"):

		# Update visibility
		get_node("empty").visible = not value
		get_node("full").visible = value