tool
extends Node

# Exported variables
export var health = 3 setget set_health
export var max_health = 5 setget set_max_health

# Classes
var health_marker = preload("res://ui/health_marker/health_marker.tscn")

# Build markers on first load
func _ready():

	rebuildMarkers()

# Rebuilds the list of health markers
func rebuildMarkers():

	# Removes all current children
	for child in get_children():
		remove_child(child)

	# Iterates through all slots to fill
	for i in range(max_health):

		# Creates a new health marker
		var child = health_marker.instance()
		add_child(child)

		# Set up variables for activity and positon
		child.active = i < health
		child.position.x = i * 20

# Sets the health
func set_health(value):

	health = value

	# Sets active children based on health
	var index = 0
	for child in get_children():

		child.active = index < health
		index += 1

# Sets health slots
func set_max_health(value):

	max_health = value
	rebuildMarkers()