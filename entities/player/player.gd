extends KinematicBody2D

# Exports
export var camera_limit = Vector2(800, 608) setget set_limit

# Movement constants
const DAMAGE_BOUNCE = 360
const DAMAGE_FLASH_COUNT = 12
const FALLING_MULTIPLIER = 2.4
const GRAVITY = Vector2(0, 980)
const JUMP_CUTOFF = -100
const JUMP_FORCE = -460
const JUMP_GRACE_TIME = 0.1
const HORIZONTAL_SPEED = 240
const INVINCIBILITY_TIME = 2

# Constants that I don't quite know what mean
const FLOOR_NORMAL = Vector2(0, -1)
const SLOPE_SLIDE_STOP = 25.0

# Movement variables
var direction = "right"
var invincibility_timer = 0
var time_spent_falling = 0
var velocity = Vector2(0, 0)

# Internal node references
onready var camera = $sprite/camera
onready var enemy_detector = $detectors/enemies
onready var pickup_detector = $detectors/pickups
onready var sprite = $sprite

# Performed on every frame
func _physics_process(delta):

	### MOVEMENT ###

	# Calculate horizontal speed and update direction
	var target_speed = 0
	if Input.is_action_pressed("ui_left"):
		direction = "left"
		target_speed += -1
	if Input.is_action_pressed("ui_right"):
		direction = "right"
		target_speed += 1

	# Move the player
	target_speed *= HORIZONTAL_SPEED
	velocity.x = lerp(velocity.x, target_speed, 0.2)

	# Calculate gravity differently if falling
	var gravity = GRAVITY
	if velocity.y > 0:
		gravity *= FALLING_MULTIPLIER

	# Add gravity to velocity
	velocity += delta * gravity

	# Move and apply physics
	velocity = move_and_slide(velocity, FLOOR_NORMAL, SLOPE_SLIDE_STOP)

	### JUMPING ###

	# Increase time spent falling if not on ground, or reset
	if is_on_floor():
		time_spent_falling = 0
	else:
		time_spent_falling += delta

	# Jump if pressing jump button
	if Input.is_action_just_pressed("ui_accept"):
		jump()
	if Input.is_action_just_released("ui_accept"):
		cancel_jump()

	### ANIMATIONS ###

	# Determine various states of being
	var is_falling = velocity.y != 0 or not is_on_floor()
	var is_running = target_speed != 0 or velocity.x != 0

	# Adapt animation to current state
	if is_falling:
		sprite.animation = "fall_%s" % direction
	elif is_running:
		sprite.animation = "walk_%s" % direction
		sprite.frames.set_animation_speed(sprite.animation, abs(velocity.x) / HORIZONTAL_SPEED * 20)
	else:
		sprite.animation = "idle_%s" % direction

	### ENEMIES ###

	# Decrease invincibility timer
	invincibility_timer = max(0, invincibility_timer - delta)

	# Flash on invincibility
	visible = int(floor(invincibility_timer * DAMAGE_FLASH_COUNT)) % 2 == 0

	# Hurt player if hitting enemy and not invincible
	for body in enemy_detector.get_overlapping_bodies():

		# Check invincibility
		if invincibility_timer == 0:

			# Set the invincibility
			invincibility_timer = INVINCIBILITY_TIME

			# Decrease health
			$sprite/camera/canvas/hud/health_status.health -= 1

			velocity = (position - body.position).normalized() * DAMAGE_BOUNCE

# Cancels the jump if the player releases the jump button
func cancel_jump():

	# Cuts off jump ONLY if we are going up fast enough
	if velocity.y < JUMP_CUTOFF:
		velocity.y = JUMP_CUTOFF

# Checks if player can jump - if so, makes them
func jump():

	# Check how long player has been falling
	if time_spent_falling < JUMP_GRACE_TIME:

		# Prevent "double jumping"
		time_spent_falling = JUMP_GRACE_TIME

		# Jump
		velocity.y = JUMP_FORCE

# Sets the camera limits
func set_limit(vec):

	# Updates limits if node can be found
	if has_node("sprite/camera"):
		var camera = get_node("sprite/camera")
		camera.limit_right = vec.x
		camera.limit_bottom = vec.y