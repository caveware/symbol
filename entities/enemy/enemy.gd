extends KinematicBody2D

# Enemy constants
const GRAVITY = Vector2(0, 980)

# Constants that I don't quite know what mean
const FLOOR_NORMAL = Vector2(0, -1)
const SLOPE_SLIDE_STOP = 25.0

# Enemy variables
var mass = 0
var velocity = Vector2(0, 0)

# Performed on physics update
func _physics_process(delta):

	# Make enemy fall
	velocity += delta * GRAVITY * mass
	velocity = move_and_slide(velocity, FLOOR_NORMAL, SLOPE_SLIDE_STOP)