extends "res://entities/enemy/enemy.gd"

# Movement constants
const MOVEMENT_SPEED = 80

# Movement variables
var direction_multiplier = 1

# Internalised nodes
onready var sprite = $sprite

# Performed when added to scene
func _ready():

	# Increase mass so falling happens
	mass = 1

	# Start horizontal movement (to the right)
	velocity.x = MOVEMENT_SPEED

# Performed when physics updated
func _physics_process(delta):

	# Update velocity
	velocity.x = direction_multiplier * MOVEMENT_SPEED

	# Store last velocity
	var last_velocity = velocity.x

	# Move horizontally
	velocity = move_and_slide(velocity, FLOOR_NORMAL, SLOPE_SLIDE_STOP)

	# Act on if velocity is different
	if velocity.x != last_velocity:
		direction_multiplier = -direction_multiplier

	# Update animation
	sprite.animation = "right" if direction_multiplier == 1 else "left"